Libressl Overlay to get most stuff working despite requiring OpenSSL

> 

To switch a stage3 from openssl to libressl one does.

>	

>	Add the overlay using layman.

>

	layman -o https://0xacab.org/emmata/skunkworks/raw/master/repo.xml -f -a skunkworks
	
>	Now:

>	
    echo 'USE="${USE} libressl"' >> /etc/portage/make.conf 
	mkdir -p /etc/portage/profile
	echo "-libressl" >> /etc/portage/profile/use.stable.mask 
	
	#those two are new!
	echo "-libressl" >> /etc/portage/profile/use.mask
	echo "-curl_ssl_libressl" >> /etc/portage/profile/use.mask
	echo "dev-libs/libressl" >> /etc/portage/package.unmask

	emerge -u gentoolkit
	emerge -f libressl 
	emerge -C openssl 
	emerge -1q libressl
	emerge @preserved-rebuild
	revdep-rebuild

> 	Optionally add libressl overlay

	layman -a libressl

>	If having further issues, consult https://wiki.gentoo.org/wiki/Project:LibreSSL
>	If you have bugs report them to upstream and not directly gentoo.
>	Please yell at whoever messed up and included SSLv3 or the new openssl crap.
>	IT may be also usefull to add the libressl overlay for your other programs.
>	Above commands assume layman is version 2.1 or newer.
